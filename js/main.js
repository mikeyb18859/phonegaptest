var PAGE_WIDTH = 800;
var PAGE_HEIGHT = 1280;
var PAGES_PATH = 'pages/';
var URLS = [
	'homepage',
	'website',
	'carousel1',
	'carousel2'
];

$(document).ready(function(e) {
		
        initApp();
		
		// SLIDER
		// var slideWidth = $('body').width();
		// var sliderWidth = slideWidth * $('#carousel div').length;
		// //$('#carousel').css('width', sliderWidth*2);

		// $('#carousel div').wrapAll('<div id=inertia_slider style="width:' + sliderWidth + 'px"></div>').css('width', slideWidth);
		
		// Draggable.create("#inertia_slider", {type:"x", edgeResistance:0.8, bounds:{
		// 	left:0,
		// 	width:slideWidth
			
		// 	}, throwProps:true,
		// snap:function(endValue) {
  //           return Math.round(endValue / slideWidth) * slideWidth;
  //       }
		
		// });
});
function initApp(){
	$('.content').hide();
	$('.current_page').html($('#p0')[0].outerHTML);

	var currPage_content = $('.current_page .content')
	currPage_content.removeAttr('id');
	currPage_content.show();

	//EVENTS:
	$('.current_page').on('click', '.button', function(event) {
		var btnId = $(this).attr('id');
			btnId = btnId[btnId.length-1];
			console.log('Button'+btnId)
			swapPage(btnId);
		
	});
	$('.current_page').on('click', '#back_btn', function(event) {
		swapPage(0);
	});
	
}
function swapPage(pageID){
	var current_page = $('.current_page');
	var next_page = $('.next_page');
	$('.next_page').html($('#p'+pageID)[0].outerHTML);

	var nextPage_content = $('.next_page .content');
	nextPage_content.removeAttr('id');
	nextPage_content.show();
	
	

	TweenLite.to(current_page,0.5,{left:-PAGE_WIDTH, force3D:true, onComplete:function(){
		console.log('DONE')
		current_page.html(next_page.html());
		current_page.css('left', 0);
		next_page.html('');
	} });
	TweenLite.from(next_page,0.5,{opacity:0,scale:0.5,force3D:true});

	
}